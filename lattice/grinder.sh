#!/bin/bash
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ] ; do SOURCE="$(readlink "$SOURCE")"; done
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
export CLASSPATH=$CLASSPATH:$HOME/jython2.5.2/jython.jar:$DIR/lib/grinder.jar
java net.grinder.Grinder
