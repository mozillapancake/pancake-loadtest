#!/bin/bash
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ] ; do SOURCE="$(readlink "$SOURCE")"; done
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
export CLASSPATH=$CLASSPATH:$DIR/lib/grinder.jar
nohup java net.grinder.Console > /dev/null 2>&1 &
