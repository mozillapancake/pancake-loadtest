import site
from net.grinder.script.Grinder import grinder
from net.grinder.script import Test
from net.grinder.plugin.http import HTTPRequest
from HTTPClient import NVPair
import simplejson as json
import time

HOST = 'http://localhost:4322'

ENDPOINTS = {
        'search' : '/lattice/%(username)s/stack/search?v=3',
        'social' : '/lattice/%(username)s/stack/social',
        'warp' : '/lattice/%(username)s/stack/warp',
        'link' : '/lattice/%(username)s/stack/link',
        'get_stack' : '/lattice/%(username)s/stack/%(stack_id)s',
        'get_nodes' : '/lattice/%(username)s/stack/%(stack_id)s/nodes',
        'create_user' : '/lattice/%(username)s/create',
        'top_rated' : '/lattice/%(username)s/stack/top_rated?v=3',
        'active_sessions' : '/lattice/%(username)s/session/active',
}

logging = grinder.logger
request = HTTPRequest(url=HOST)

def __get_username():
    #thread_number = grinder.threadNumber
    return 'testuser-%s-%d@mozilla.com' % (grinder.processName, grinder.runNumber)

def __url_args(**kwargs):
    output = {'username' : __get_username()}
    for k,v in kwargs.items():
        output.update({k : v})
    return output

def create_user():
    result = request.POST(ENDPOINTS['create_user'] % __url_args())
    return result

def create_with(data, type='search'):
    resp = request.POST(ENDPOINTS[type] % __url_args(), json.dumps(data))
    return json.loads(resp.text)

def link(data, type='link'):
    resp = request.PUT(ENDPOINTS[type] % __url_args(), json.dumps(data))
    data = resp.text
    if data:
        return json.loads(resp.text)

def resume_by(data, type='search'):
    resp = request.PUT(ENDPOINTS[type] % __url_args(), json.dumps(data))
    return json.loads(resp.text)

def get_nodes(stack_id):
    resp = request.GET(ENDPOINTS['get_nodes'] % __url_args(stack_id=stack_id))
    return json.loads(resp.text)

def get_active_sessions():
    resp = request.GET(ENDPOINTS['active_sessions'] % __url_args())
    if resp.statusCode == 200:
        try:
            data = json.loads(resp.text)
        except:
            logging.error('JSONDecodeError with input: "%s"' % resp.text)
        return data

class TestRunner(object):

    def __call__(self):
        Test(1, 'user_create').wrap(create_user)()
        Test(1, 'user_create').wrap(get_active_sessions)()

        # Test 2
        stack_data = Test(2, 'stack_create: create').wrap(create_with)({
            'search_url' : 'http://www.bing.com',
            'search_terms' : 'awesome cello music',
            'place_url' : 'http://ask.metafilter.com/58679/Searching-for-cello-music',
            'place_title' : 'Searching for cello music - music cello  | Ask MetaFilter'
        })
        Test(2, 'stack_create: get_active_sessions').wrap(get_active_sessions)()
        Test(2, 'stack_create: get_nodes').wrap(get_nodes)(stack_data['stack']['id'])

        # Test 3
        stack_data = Test(3, 'topic_search_1: create').wrap(create_with)({
            'search_url' : 'http://www.bing.com',
            'search_terms' : 'instagram',
            'place_url' : 'http://bits.blogs.nytimes.com/2012/04/09/facebook-acquires-photo-sharing-service-instagram/',
            'place_title' : 'Facebook to Buy Photo-Sharing Service Instagram for $1 Billion'
        }, type='search')
        Test(3, 'topic_search_1: get_active_sessions').wrap(get_active_sessions)()
        Test(3, 'topic_search_1: get_nodes').wrap(get_nodes)(stack_data['stack']['id'])
        # warp
        Test(3, 'topic_search_1: warp 1').wrap(link)({
            'stack_id' : stack_data['stack']['id'],
            'session_id' : stack_data['session_id'],
            'place_id' : stack_data['search_place']['id']
        }, type='warp')
        Test(3, 'topic_search_1: get_nodes').wrap(get_nodes)(stack_data['stack']['id'])

        # links
        Test(3, 'topic_search_1: link 1').wrap(link)({
            'stack_id' : stack_data['stack']['id'],
            'session_id' : stack_data['session_id'],
            'place_url' : 'http://news.cnet.com/8301-19882_3-57411413-250/why-facebook-plunked-down-$1-billion-to-buy-instagram/',
            'place_title' : "Why Facebook plunked down $1 billion to buy Instagram | Rafe's Radar - CNET News"
        })
        Test(3, 'topic_search_1: link 2').wrap(link)({
            'stack_id' : stack_data['stack']['id'],
            'session_id' : stack_data['session_id'],
            'place_url' : 'http://news.cnet.com/8301-11386_3-57411203-76/space-station-snaps-photo-of-robotic-cargo-craft/',
            'place_title' : 'Space station snaps photo of robotic cargo craft | Cutting Edge - CNET News'
        })
        Test(3, 'topic_search_1: link 3').wrap(link)({
            'stack_id' : stack_data['stack']['id'],
            'session_id' : stack_data['session_id'],
            'place_url' : 'http://reviews.cnet.com/2300-9150_7-10011897.html',
            'place_title' : 'Nest Learning Thermostat - Top-rated reviews of the week (photos) - CNET Reviews'
        })
        Test(3, 'topic_search_1: get_nodes').wrap(get_nodes)(stack_data['stack']['id'])

        # Test 4
        stack_data = Test(4, 'social_post: create').wrap(create_with)({
            'service_url' : 'https://twitter.com',
            'friend_url' : 'https://twitter.com/#!/neilhimself',
            'friend_name' : 'Neil Gaiman',
            'place_url' : 'http://vimeo.com/39921254',
            'place_title' : 'vimeo.com/39921254'
        }, type='social')
        Test(4, 'social_post: get_active_sessions').wrap(get_active_sessions)()
        Test(4, 'social_post: get_nodes').wrap(get_nodes)(stack_data['stack']['id'])

        Test(4, 'social_post: link 1').wrap(link)({
            'stack_id' : stack_data['stack']['id'],
            'session_id' : stack_data['session_id'],
            'place_url' : 'http://www.engadget.com/2011/11/07/panasonic-launches-lumix-gx1-micro-four-thirds-camera-we-go-han/',
            'place_title' : 'Panasonic launches Lumix DMC-GX1 Micro Four Thirds camera, we go hands-on -- Engadget'
        })
        stack_data = Test(4, 'social_post: new post').wrap(create_with)({
            'service_url' : 'https://twitter.com',
            'friend_url' : 'https://twitter.com/#!/neilhimself',
            'friend_name' : 'Neil Gaiman',
            'place_url' : 'http://www.nerdist.com/2012/04/its-tom-lehrers-birthday/',
            'place_title' : "Nerdist : It's Tom Lehrer's Birthday"
        }, type='social')
        Test(4, 'social_post: link 2').wrap(link)({
            'stack_id' : stack_data['stack']['id'],
            'session_id' : stack_data['session_id'],
            'place_url' : 'http://www.nerdist.com/',
            'place_title' : 'Nerdist'
        })
        Test(4, 'social_post: link 3').wrap(link)({
            'stack_id' : stack_data['stack']['id'],
            'session_id' : stack_data['session_id'],
            'place_url' : 'http://www.nerdist.com/2012/04/pop-my-culture-75-felicia-day/',
            'place_title' : 'Nerdist : Pop My Culture #75: Felicia Day'
        })

        # Test 5
        stack_data = Test(5, 'topic_search_2: create').wrap(create_with)({
            'search_url' : 'http://www.bing.com',
            'search_terms' : 'toronto thai food',
            'place_url' : 'http://www.torontolife.com/daily/daily-dish/from-the-print-edition-daily-dish/2012/03/28/where-to-eat-now-2012-food-trucks/',
            'place_title' : 'Where to Eat Now 2012: the 10 top food trucks in the GTA'
        }, type='search')
        Test(5, 'topic_search_2: get_active_sessions').wrap(get_active_sessions)()
        Test(5, 'topic_search_2: get_nodes').wrap(get_nodes)(stack_data['stack']['id'])

        Test(5, 'topic_search_2: link 1').wrap(link)({
            'stack_id' : stack_data['stack']['id'],
            'session_id' : stack_data['session_id'],
            'place_url' : 'http://www.torontolife.com/',
            'place_title' : "Home | torontolife.com"
        })
        # gonna come back here later
        link_data = Test(5, 'topic_search_2: link 2').wrap(link)({
            'stack_id' : stack_data['stack']['id'],
            'session_id' : stack_data['session_id'],
            'place_url' : 'http://www.torontolife.com/daily/informer/from-print-edition-informer/2012/02/15/almost-rich/',
            'place_title' : 'Almost Rich: an examination of the true cost of city living and why rich is never rich enough | From the Print Edition | torontolife.com'
        })
        Test(5, 'topic_search_2: get_nodes').wrap(get_nodes)(stack_data['stack']['id'])

        Test(5, 'topic_search_2: link 3').wrap(link)({
            'stack_id' : stack_data['stack']['id'],
            'session_id' : stack_data['session_id'],
            'place_url' : 'http://www.torontolife.com/daily/informer/from-print-edition-informer/2012/02/15/almost-rich/5/',
            'place_title' : 'Almost Rich: an examination of the true cost of city living and why rich is never rich enough | From the Print Edition | torontolife.com'
        })
        Test(5, 'topic_search_2: link 4').wrap(link)({
            'stack_id' : stack_data['stack']['id'],
            'session_id' : stack_data['session_id'],
            'place_url' : 'http://www.torontolife.com/daily/informer/from-print-edition-informer/2012/02/15/almost-rich/6/',
            'place_title' : 'Almost Rich: an examination of the true cost of city living and why rich is never rich enough | From the Print Edition | torontolife.com'
        })
        Test(5, 'topic_search_2: get_nodes').wrap(get_nodes)(stack_data['stack']['id'])

        Test(5, 'topic_search_2: link 5').wrap(link)({
            'stack_id' : stack_data['stack']['id'],
            'session_id' : stack_data['session_id'],
            'place_url' : 'http://www.torontolife.com/daily/informer/from-print-edition-informer/2012/02/15/almost-rich/7/',
            'place_title' : 'Almost Rich: an examination of the true cost of city living and why rich is never rich enough | From the Print Edition | torontolife.com'
        })
        Test(5, 'topic_search_2: warp 1').wrap(link)({
            'stack_id' : stack_data['stack']['id'],
            'session_id' : stack_data['session_id'],
            'place_id' : link_data['place_id'],
        }, type='warp')
        Test(5, 'topic_search_2: link 6').wrap(link)({
            'stack_id' : stack_data['stack']['id'],
            'session_id' : stack_data['session_id'],
            'place_url' : 'http://www.torontolife.com/daily/informer/from-print-edition-informer/2012/02/15/almost-rich/7/',
            'place_title' : 'Almost Rich: an examination of the true cost of city living and why rich is never rich enough | From the Print Edition | torontolife.com'
        })
        Test(5, 'topic_search_2: get_nodes').wrap(get_nodes)(stack_data['stack']['id'])

        Test(5, 'topic_search_2: link 7').wrap(link)({
            'stack_id' : stack_data['stack']['id'],
            'session_id' : stack_data['session_id'],
            'place_url' : 'http://www.torontolife.com/',
            'place_title' : "Home | torontolife.com"
        })
        session_data = Test(5, 'topic_search_2: resume by search').wrap(link)({
            'stack_id' : stack_data['stack']['id'],
            'place_id' : link_data['place_id'],
            'search_terms' : 'toronto life'
        }, type='search')
        stack_data['session_id'] = session_data['session_id']

        Test(5, 'topic_search_2: link 8').wrap(link)({
            'stack_id' : stack_data['stack']['id'],
            'session_id' : stack_data['session_id'],
            'place_url' : 'http://www.torontolife.com/daily/daily-dish/neighbourhoods/2010/06/10/the-danforth-guide-our-21-favourite-spots-along-the-east-ends-main-avenue/2/',
            'place_title' : "The Danforth Guide: our 21 favourite spots along the east end's main avenue | Neighbourhoods | torontolife.com"
        })
        Test(5, 'topic_search_2: link 9').wrap(link)({
            'stack_id' : stack_data['stack']['id'],
            'session_id' : stack_data['session_id'],
            'place_url' : 'http://www.torontolife.com/daily/daily-dish/neighbourhoods/2010/06/10/the-danforth-guide-our-21-favourite-spots-along-the-east-ends-main-avenue/3/',
            'place_title' : "The Danforth Guide: our 21 favourite spots along the east end's main avenue | Neighbourhoods | torontolife.com"
        })
        Test(5, 'topic_search_2: link 10').wrap(link)({
            'stack_id' : stack_data['stack']['id'],
            'session_id' : stack_data['session_id'],
            'place_url' : 'http://www.torontolife.com/daily/daily-dish/neighbourhoods/2010/06/10/the-danforth-guide-our-21-favourite-spots-along-the-east-ends-main-avenue/4/',
            'place_title' : "The Danforth Guide: our 21 favourite spots along the east end's main avenue | Neighbourhoods | torontolife.com"
        })
        Test(5, 'topic_search_2: get_nodes').wrap(get_nodes)(stack_data['stack']['id'])

        Test(5, 'topic_search_2: link 11').wrap(link)({
            'stack_id' : stack_data['stack']['id'],
            'session_id' : stack_data['session_id'],
            'place_url' : 'http://www.torontolife.com/daily/daily-dish/neighbourhoods/2010/06/10/the-danforth-guide-our-21-favourite-spots-along-the-east-ends-main-avenue/5/',
            'place_title' : "The Danforth Guide: our 21 favourite spots along the east end's main avenue | Neighbourhoods | torontolife.com"
        })
        Test(5, 'topic_search_2: link 12').wrap(link)({
            'stack_id' : stack_data['stack']['id'],
            'session_id' : stack_data['session_id'],
            'place_url' : 'http://www.torontolife.com/daily/daily-dish/neighbourhoods/2010/06/10/the-danforth-guide-our-21-favourite-spots-along-the-east-ends-main-avenue/6/',
            'place_title' : "The Danforth Guide: our 21 favourite spots along the east end's main avenue | Neighbourhoods | torontolife.com"
        })
        Test(5, 'topic_search_2: link 13').wrap(link)({
            'stack_id' : stack_data['stack']['id'],
            'session_id' : stack_data['session_id'],
            'place_url' : 'http://www.torontolife.com/daily/daily-dish/neighbourhoods/2010/06/10/the-danforth-guide-our-21-favourite-spots-along-the-east-ends-main-avenue/7/',
            'place_title' : "The Danforth Guide: our 21 favourite spots along the east end's main avenue | Neighbourhoods | torontolife.com"
        })
        Test(5, 'topic_search_2: get_nodes').wrap(get_nodes)(stack_data['stack']['id'])

        Test(5, 'topic_search_2: link 14').wrap(link)({
            'stack_id' : stack_data['stack']['id'],
            'session_id' : stack_data['session_id'],
            'place_url' : 'http://www.torontolife.com/daily/daily-dish/neighbourhoods/2010/06/10/the-danforth-guide-our-21-favourite-spots-along-the-east-ends-main-avenue/8/',
            'place_title' : "The Danforth Guide: our 21 favourite spots along the east end's main avenue | Neighbourhoods | torontolife.com"
        })
        Test(5, 'topic_search_2: link 15').wrap(link)({
            'stack_id' : stack_data['stack']['id'],
            'session_id' : stack_data['session_id'],
            'place_url' : 'http://www.torontolife.com/daily/daily-dish/neighbourhoods/2010/06/10/the-danforth-guide-our-21-favourite-spots-along-the-east-ends-main-avenue/9/',
            'place_title' : "The Danforth Guide: our 21 favourite spots along the east end's main avenue | Neighbourhoods | torontolife.com"
        })
        Test(5, 'topic_search_2: link 16').wrap(link)({
            'stack_id' : stack_data['stack']['id'],
            'session_id' : stack_data['session_id'],
            'place_url' : 'http://www.torontolife.com/daily/daily-dish/neighbourhoods/2010/06/10/the-danforth-guide-our-21-favourite-spots-along-the-east-ends-main-avenue/10/',
            'place_title' : "The Danforth Guide: our 21 favourite spots along the east end's main avenue | Neighbourhoods | torontolife.com"
        })
        Test(5, 'topic_search_2: link 17').wrap(link)({
            'stack_id' : stack_data['stack']['id'],
            'session_id' : stack_data['session_id'],
            'place_url' : 'http://www.torontolife.com/daily/daily-dish/neighbourhoods/2010/06/10/the-danforth-guide-our-21-favourite-spots-along-the-east-ends-main-avenue/11/',
            'place_title' : "The Danforth Guide: our 21 favourite spots along the east end's main avenue | Neighbourhoods | torontolife.com"
        })
        Test(5, 'topic_search_2: get_nodes').wrap(get_nodes)(stack_data['stack']['id'])
         
        Test(5, 'topic_search_2: link 18').wrap(link)({
            'stack_id' : stack_data['stack']['id'],
            'session_id' : stack_data['session_id'],
            'place_url' : 'http://www.torontolife.com/daily/daily-dish/neighbourhoods/2010/06/10/the-danforth-guide-our-21-favourite-spots-along-the-east-ends-main-avenue/12/',
            'place_title' : "The Danforth Guide: our 21 favourite spots along the east end's main avenue | Neighbourhoods | torontolife.com"
        })
        Test(5, 'topic_search_2: link 19').wrap(link)({
            'stack_id' : stack_data['stack']['id'],
            'session_id' : stack_data['session_id'],
            'place_url' : 'http://www.torontolife.com/daily/daily-dish/neighbourhoods/2010/06/10/the-danforth-guide-our-21-favourite-spots-along-the-east-ends-main-avenue/13/',
            'place_title' : "The Danforth Guide: our 21 favourite spots along the east end's main avenue | Neighbourhoods | torontolife.com"
        })
        Test(5, 'topic_search_2: get_nodes').wrap(get_nodes)(stack_data['stack']['id'])
